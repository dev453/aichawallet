package com.sensoft.aichawallet

import android.app.Application
import android.content.Context
import android.content.IntentFilter
import android.net.ConnectivityManager
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.multidex.MultiDex
import com.orhanobut.logger.AndroidLogAdapter
import com.orhanobut.logger.Logger
import com.sensoft.aichawallet.di.AppModule
import com.sensoft.aichawallet.di.DaggerAppComponent
import com.sensoft.aichawallet.helpers.network.ConnectivityReceiver
import com.sensoft.aichawallet.token.TokenJWTService
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import javax.inject.Inject

class AichaAppClass : Application(), ConnectivityReceiver.ConnectivityReceiverListener, AppLifecycleObserver.AppLifecycleListener, ApiErrorListener {

    @Inject
    lateinit var appLifecycleObserver: AppLifecycleObserver

    @Inject
    lateinit var tokenJWTService: TokenJWTService

    private var apiErrorListener: ApiErrorListener? = null


    override fun onCreate() {
        super.onCreate()
        Logger.addLogAdapter(AndroidLogAdapter())

        DaggerAppComponent
            .builder()
            .appModule(AppModule(this))
            .build()
            .inject(this)

        appInstance = this

        ViewPump.init(
            ViewPump.builder()
                .addInterceptor(
                    CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                            .setDefaultFontPath("bank_font/GoogleSans-Regular.ttf")
                            .setFontAttrId(R.attr.fontPath)
                            .build()
                    )).build()

        )

        ProcessLifecycleOwner.get().lifecycle.addObserver(appLifecycleObserver)
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    fun isLogged(): Boolean {
        return tokenJWTService.hasTokenValid()
    }

    fun getToken(): String {
        return tokenJWTService.getToken()
    }


    fun setInternetConnectionListener(listener: ApiErrorListener?) {
        apiErrorListener = listener
    }

    fun hasInternetConnectionListener(): Boolean {
        return apiErrorListener != null
    }


    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
    override fun onUnauthorized() {
        apiErrorListener?.onUnauthorized()
    }

    override fun onInternetUnavailable() {
        apiErrorListener?.onInternetUnavailable()
    }

    override fun onInternetAvailable() {
        apiErrorListener?.onInternetAvailable()
    }

    override fun onEnteringForeground() {
        isDeviceBackground = false
        observeConnectivity()
    }

    override fun onEnteringBackGround() {
        isDeviceBackground = true
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isDeviceConnected = isConnected
    }

    private fun observeConnectivity() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }


    companion object {
        private lateinit var appInstance: AichaAppClass
        var isDeviceConnected = false
        var isDeviceBackground = false
        fun getAppInstance(): AichaAppClass {
            return appInstance
        }
    }
}
interface ApiErrorListener {
    fun onInternetUnavailable()
    fun onInternetAvailable()
    fun onUnauthorized()
}