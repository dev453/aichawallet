/*
 * Copyright (c) 2020 by Appndigital, Inc.
 * All Rights Reserved
 */

package com.sensoft.aichawallet

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent

class AppLifecycleObserver(app: AichaAppClass) : LifecycleObserver {

    var appLifecycleListener: AppLifecycleListener = app

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onEnterForeground() {
       appLifecycleListener.onEnteringForeground()
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onEnterBackground() {
        appLifecycleListener.onEnteringBackGround()
    }

    interface AppLifecycleListener {
        fun onEnteringForeground()
        fun onEnteringBackGround()
    }
}