package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_question.*
import kotlinx.android.synthetic.main.bank_item_questionanswer.view.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankQuestionFragment : BankBaseFragment() {

    val mQuestionArray: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_question, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_questions_answers)

        val questionAdapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_questionanswer,
            onViewClicked = { _, item, position ->

            },
            onBind = { bindView, item, position ->

                bindView.txtQuestion.text = mQuestionArray[position].modelQuestion

                bindView.viewAnswer.setOnClickListener {
                    bindView.txtQuestionDesc.visibility = View.VISIBLE
                    bindView.hideAnswer.visibility = View.VISIBLE
                    bindView.viewAnswer.visibility = View.GONE
                }
                bindView.hideAnswer.setOnClickListener {
                    bindView.txtQuestionDesc.visibility = View.GONE
                    bindView.hideAnswer.visibility = View.GONE
                    bindView.viewAnswer.visibility = View.VISIBLE
                }
            })

        questionRecyclerview.adapter = questionAdapter
        questionRecyclerview.layoutManager = LinearLayoutManager(context)
        questionAdapter.addItems(addQuestion())

        setStatusbar()
    }

    private fun addQuestion(): ArrayList<BankModel> {

        val model1 = BankModel()
        model1.modelQuestion = resources.getString(R.string.bank_question1)
        mQuestionArray.add(model1)

        val model2 = BankModel()
        model2.modelQuestion = resources.getString(R.string.bank_question2)
        mQuestionArray.add(model2)

        val model3 = BankModel()
        model3.modelQuestion = resources.getString(R.string.bank_question3)
        mQuestionArray.add(model3)

        val model4 = BankModel()
        model4.modelQuestion = resources.getString(R.string.bank_question4)
        mQuestionArray.add(model4)

        val model5 = BankModel()
        model5.modelQuestion = resources.getString(R.string.bank_question5)
        mQuestionArray.add(model5)

        val model6 = BankModel()
        model6.modelQuestion = resources.getString(R.string.bank_question6)
        mQuestionArray.add(model6)

        return mQuestionArray
    }

    private fun setStatusbar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorBackgroundFragment)
                ?.let { window.statusBarColor = it }
        }
    }
}
