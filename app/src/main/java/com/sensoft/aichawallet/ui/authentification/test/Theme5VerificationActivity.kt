package com.sensoft.aichawallet.ui.authentification.test

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.helpers.utils.Theme5OTPEditText
import com.sensoft.aichawallet.helpers.utils.launchActivity
import com.sensoft.aichawallet.helpers.utils.onClick
import kotlinx.android.synthetic.main.theme5_activity_otp.*

class Theme5VerificationActivity : BaseActivity() {
    private var mEds: Array<EditText?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.theme5_activity_otp)
        mEds = arrayOf(edDigit1, edDigit2, edDigit3, edDigit4)
        Theme5OTPEditText(
            mEds!!,
            this
        )
        mEds!!.forEachIndexed { _, editText ->
            editText?.onFocusChangeListener = focusChangeListener
        }
        btnContinue.onClick {
            launchActivity<Theme5PasswordSetActivity>()
        }
        ivBack.onClick {
            finish()
        }
    }
    private val focusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

    }

}
