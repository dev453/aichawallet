package com.sensoft.aichawallet.ui.authentification.sign_in

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.ui.HomeActivity
import com.sensoft.aichawallet.ui.authentification.password_manager.ForgetPasswordActivity
import kotlinx.android.synthetic.main.activity_signin.*

class PaymentSigninActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)

        payment_signin_btnSignIn.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }

        signin_tvForgetPassword.setOnClickListener {
            val intent = Intent(this, ForgetPasswordActivity::class.java)
            startActivity(intent)
        }
    }

    fun showHidePassword(view: View) {
        if (view.id == R.id.signInSHow) {
            if (edtSignInPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                signInSHow.setImageResource(R.drawable.bank_ic_visibility_black_24dp)
                edtSignInPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance();
            } else {
                signInSHow.setImageResource(R.drawable.bank_ic_visibility_off_black_24dp)
                edtSignInPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }
    }
}
