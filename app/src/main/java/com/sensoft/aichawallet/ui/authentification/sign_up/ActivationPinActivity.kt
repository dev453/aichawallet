package com.sensoft.aichawallet.ui.authentification.sign_up

import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.di.GlobalInjectorModule
import com.sensoft.aichawallet.helpers.utils.launchActivity
import com.sensoft.aichawallet.helpers.utils.onClick
import com.sensoft.aichawallet.ui.HomeActivity
import com.sensoft.aichawallet.ui.authentification.ActivationAccountViewModel
import com.sensoft.aichawallet.ui.authentification.di.DaggerActivationPinComponent
import kotlinx.android.synthetic.main.theme5_activity_password_set.*
import javax.inject.Inject

class ActivationPinActivity : BaseActivity() {

    lateinit var viewModel: ActivationAccountViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation_pin)
        /*
        DaggerActivationPinComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(ActivationPinViewModel::class.java)*/
        btnContinue.onClick {
            launchActivity <HomeActivity>()
        }
        ivBack.onClick {
            finish()
        }
    }
}