package com.sensoft.aichawallet.ui.authentification

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import com.sensoft.aichawallet.core.BaseViewModel
import com.sensoft.aichawallet.models.activation.request.ActivationData
import com.sensoft.aichawallet.models.activation.request.ActivationRepository
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class AuthViewModel @Inject constructor(val context: Context, private val activationRepository: ActivationRepository) :
    BaseViewModel() {
    private val liveResponseActivation = MutableLiveData<ResponseActivationRequest>()
    val error = MutableLiveData<String>()

    fun onActivationDone():LiveData<ResponseActivationRequest>{
        return liveResponseActivation
    }

    fun onErrorInActivation():LiveData<String>{
        return error
    }

    fun activateRequest(activationData: ActivationData){
        activationRepository.run {
            activateRequest(activationData).subscribeBy(
                onError = {
                    error.postValue(it.localizedMessage)
                },
                onSuccess = {
                    liveResponseActivation.value=it
                }
            )
        }
    }

    companion object {
        private const val TAG = "AuthViewModel"
    }
}