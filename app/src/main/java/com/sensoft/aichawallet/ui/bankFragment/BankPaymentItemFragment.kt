package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import android.widget.ImageView

import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_fragment_paymentitem.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*


class BankPaymentItemFragment : BankBaseFragment() {

    var mBankPaymentHistoryFragment = BankPaymentHistoryFragment()
    var mBankPaymentInvoiceFragment = BankPaymentInvoiceFragment()
    var mBankPaymentFragment = BankPaymentFragment()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View = inflater.inflate(R.layout.bank_fragment_paymentitem, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankPaymentFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_payment_electricity_title)

        History.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, mBankPaymentHistoryFragment)
                ?.commit()
        }

        Invoice.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, mBankPaymentInvoiceFragment)
                ?.commit()
        }

        val bundle = arguments
        if (bundle != null) {
            txtMenuItemTitle.text = bundle.get("title").toString()
        }
        setStatusBar()
    }

    private fun setStatusBar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorBackgroundFragment)
                ?.let { window.statusBarColor = it }
        }
    }

}
