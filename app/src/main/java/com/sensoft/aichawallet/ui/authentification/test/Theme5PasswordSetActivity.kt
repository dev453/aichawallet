package com.sensoft.aichawallet.ui.authentification.test

import android.os.Bundle
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.helpers.utils.launchActivity
import com.sensoft.aichawallet.helpers.utils.onClick
import com.sensoft.aichawallet.ui.HomeActivity
import kotlinx.android.synthetic.main.theme5_activity_password_set.*

class Theme5PasswordSetActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.theme5_activity_password_set)
        btnContinue.onClick {
            launchActivity <HomeActivity>()
        }
        ivBack.onClick {
            finish()
        }
    }
}
