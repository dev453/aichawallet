package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_beneficiary_list.*
import kotlinx.android.synthetic.main.bank_item_beneficiarylist.view.*


class BankBeneficiaryListFragment : BankBaseFragment() {
    val mUserList: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.bank_fragment_beneficiary_list, container, false
    )

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        var alphabet: Char = 'A'
        while (alphabet <= 'Z') {
            txtAlphabeticView.append(alphabet.toString().plus("\n"))
            ++alphabet
        }

        setStatusBar()
        imgBeneficieryBack.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankTransferFragment())?.commit()
        }

        val recyclerviewadapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_beneficiarylist,
            onBind = { view, _, position ->
                view.userImage.setImageResource(mUserList[position].modelUserImage)
                view.username.text = mUserList[position].modelUserName
                view.userAccountNumber.text = mUserList[position].modelUserAccountNumber
                view.userBankName.text = mUserList[position].modelUserBankName
            },
            onViewClicked = { view, _, _ ->
                view.setOnClickListener {

                }
            })

        bankUserRecyclerview.adapter = recyclerviewadapter
        bankUserRecyclerview.layoutManager = LinearLayoutManager(context)
        recyclerviewadapter.addItems(addUserDetails())
    }

    private fun addUserDetails(): ArrayList<BankModel> {

        val model = BankModel()
        model.modelUserImage = R.drawable.bank_ic_user
        model.modelUserName = "New Beneficiary"
        model.modelUserAccountNumber = "111 222 33333"
        model.modelUserBankName = "AICHA WALLET"
        mUserList.add(model)

        val model1 = BankModel()
        model1.modelUserImage = R.drawable.bank_ic_user1
        model1.modelUserName = "Laura smith"
        model1.modelUserAccountNumber = "111 222 33333"
        model1.modelUserBankName = "AICHA WALLET"
        mUserList.add(model1)

        val model2 = BankModel()
        model2.modelUserImage = R.drawable.bank_ic_user2
        model2.modelUserName = "Adam Zampa"
        model2.modelUserAccountNumber = "111 222 33333"
        model2.modelUserBankName = "New york Bnak"
        mUserList.add(model2)

        val model3 = BankModel()
        model3.modelUserImage = R.drawable.bank_ic_user4
        model3.modelUserName = "Michell Mars"
        model3.modelUserAccountNumber = "111 222 33333"
        model3.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model3)

        val model4 = BankModel()
        model4.modelUserImage = R.drawable.bank_ic_user4
        model4.modelUserName = "Andy William"
        model4.modelUserAccountNumber = "111 222 33333"
        model4.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model4)

        val model5 = BankModel()
        model5.modelUserImage = R.drawable.bank_ic_user5
        model5.modelUserName = "Billey Crudup"
        model5.modelUserAccountNumber = "111 222 33333"
        model5.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model5)

        val model6 = BankModel()
        model6.modelUserImage = R.drawable.bank_ic_user5
        model6.modelUserName = "Billey Crudup"
        model6.modelUserAccountNumber = "111 222 33333"
        model6.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model6)

        val model7 = BankModel()
        model7.modelUserImage = R.drawable.bank_ic_user5
        model7.modelUserName = "Billey Crudup"
        model7.modelUserAccountNumber = "111 222 33333"
        model5.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model7)

        val model8 = BankModel()
        model8.modelUserImage = R.drawable.bank_ic_user5
        model8.modelUserName = "Billey Crudup"
        model8.modelUserAccountNumber = "111 222 33333"
        model8.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model8)

        val model9 = BankModel()
        model9.modelUserImage = R.drawable.bank_ic_user5
        model9.modelUserName = "Billey Crudup"
        model9.modelUserAccountNumber = "111 222 33333"
        model9.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model9)

        val model10 = BankModel()
        model10.modelUserImage = R.drawable.bank_ic_user5
        model10.modelUserName = "Billey Crudup"
        model10.modelUserAccountNumber = "111 222 33333"
        model10.modelUserBankName = "Union Bankk, LA"
        mUserList.add(model10)

        return mUserList
    }

    private fun setStatusBar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorBackgroundFragment)
                ?.let { window.statusBarColor = it }
        }
    }
}



