package com.sensoft.aichawallet.ui.authentification.password_manager

import android.content.Intent
import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.View
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.ui.HomeActivity
import kotlinx.android.synthetic.main.activity_create_password.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class CreatePasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_password)

        txtMenuItemTitle.text = resources.getString(R.string.bank_lbl_txtForgetPasswordTitle)

        imgMenuBackImageView.setOnClickListener {
            onBackPressed()
        }

        btnSignIn.setOnClickListener {
            startActivity(Intent(this, HomeActivity::class.java))
        }
    }

    fun showHideCreatePassword(view: View) {
        if (view.id == R.id.showHideimage) {
            if (edtCreateNewPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                showHideimage.setImageResource(R.drawable.bank_ic_visibility_black_24dp)
                edtCreateNewPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance();
            } else {
                showHideimage.setImageResource(R.drawable.bank_ic_visibility_off_black_24dp)
                edtCreateNewPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

        if (view.id == R.id.showHideimage2) {
            if (edtActivityConfirmPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                showHideimage2.setImageResource(R.drawable.bank_ic_visibility_black_24dp)
                edtActivityConfirmPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance();
            } else {
                showHideimage2.setImageResource(R.drawable.bank_ic_visibility_off_black_24dp)
                edtActivityConfirmPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }
    }
}
