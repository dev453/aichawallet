package com.sensoft.aichawallet.ui.authentification.password_manager

import android.content.Intent
import android.os.Bundle
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.R
import kotlinx.android.synthetic.main.activity_forget_password.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class ForgetPasswordActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forget_password)

        txtMenuItemTitle.text = resources.getString(R.string.bank_lbl_txtForgetPasswordTitle)

        imgMenuBackImageView.setOnClickListener {
            onBackPressed()
        }

        txtResend.setOnClickListener {
            var intent = Intent(this, ResendActivity::class.java)
            startActivity(intent)
        }

        btnNext_ForgetPassword.setOnClickListener {
            var intent = Intent(this, CreatePasswordActivity::class.java)
            startActivity(intent)
        }
    }
}
