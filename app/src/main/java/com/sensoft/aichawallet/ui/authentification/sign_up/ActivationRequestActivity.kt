package com.sensoft.aichawallet.ui.authentification.sign_up

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.sensoft.aichawallet.Constants
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.di.GlobalInjectorModule
import com.sensoft.aichawallet.models.activation.request.ActivationData
import com.sensoft.aichawallet.ui.HomeActivity
import com.sensoft.aichawallet.ui.authentification.AuthViewModel
import com.sensoft.aichawallet.ui.authentification.di.DaggerActivationRequestComponent
import kotlinx.android.synthetic.main.activity_activation_request.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*
import javax.inject.Inject

class ActivationRequestActivity : BaseActivity() {

    lateinit var viewModel:AuthViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_activation_request)
        ccp_identity.registerCarrierNumberEditText(txt_wallet_id)
        txtMenuItemTitle.text = resources.getString(R.string.bank_lbl_txtActivationRequestTitle)

        DaggerActivationRequestComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)
        viewModel = ViewModelProvider(this, viewModelFactory).get(AuthViewModel::class.java)

        viewModel.onActivationDone().observe(this, Observer {
            Log.d("RESPONSEACTIVATION",it.toString())
            hideProgressDialog()
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        })

        viewModel.onErrorInActivation().observe(this, Observer {
            hideProgressDialog()
            showErrorFlashBar("erreur")
            if (it != null)
                Log.d("ERROR",it.toString())

        })


        btnNext.setOnClickListener {
            showProgressDialog()
            viewModel.activateRequest(ActivationData(ccp_identity.fullNumber, Constants.WALLET_PROVIDER))
        }
    }

}