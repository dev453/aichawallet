package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankUserCardAdapter
import kotlinx.android.synthetic.main.bank_fragment_payinvoice.*
import kotlinx.android.synthetic.main.bank_fragment_transfer.dotIndicator
import kotlinx.android.synthetic.main.bank_fragment_transfer.vpCardDetails
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankPayInvoiceFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_payinvoice, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankInvoiceFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_pay_invoice)

        btnPay.setOnClickListener {

        }
        val bankUserCardAdapter = BankUserCardAdapter(context)
        vpCardDetails.adapter = bankUserCardAdapter

        val mDotsCount = bankUserCardAdapter.count
        val mDot = arrayOfNulls<ImageView>(mDotsCount)

        for (i in 0 until mDotsCount) {
            mDot[0]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_active_dot
                )
            })
            mDot[i] = ImageView(context)
            mDot[i]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_non_active_dot
                )
            })
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            dotIndicator.addView(mDot[i], params)
        }

        vpCardDetails.clipToPadding = false
        vpCardDetails.setPadding(resources.getDimension(R.dimen._12sdp).toInt(), 0, resources.getDimension(R.dimen._40sdp).toInt(), 0)
        vpCardDetails.pageMargin = 40

        vpCardDetails.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until mDotsCount) {
                    mDot[i]?.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.bank_non_active_dot
                        )
                    })
                }
                mDot[position]?.setImageDrawable(context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.bank_active_dot
                    )
                })
            }
        })
        setStatusbar()
    }
    private fun setStatusbar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorWhitePure)
                ?.let { window.statusBarColor = it }
        }
    }
}
