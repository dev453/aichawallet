package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_news.*
import kotlinx.android.synthetic.main.bank_item_news.view.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankNewsFragment : BankBaseFragment() {

    val mNewsArray : ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_news, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }
        txtMenuItemTitle.text = resources.getString(R.string.bank_news)

        val mNewsAdapter = BankRecyclerViewGenericAdapter(R.layout.bank_item_news,onBind = {view, item, position ->
            view.txtNewsTitle.text = mNewsArray[position].modelNewsTitle
            view.imgNewsImage.setImageResource(mNewsArray[position].modelNewsImage)
        }
            ,onViewClicked = {view, item, position ->  })

        newsRecyclerview.adapter = mNewsAdapter
        newsRecyclerview.layoutManager = LinearLayoutManager(context)
        mNewsAdapter.addItems(addNews())
    }

    private fun addNews(): java.util.ArrayList<BankModel> {

        val model1 = BankModel()
        model1.modelNewsTitle = resources.getString(R.string.bank_newsCardTitle1)
        model1.modelNewsImage = R.drawable.bank_ic_payment_activity_image1
        mNewsArray.add(model1)

        val model2 = BankModel()
        model2.modelNewsTitle = resources.getString(R.string.bank_newsCardTitle1)
        model2.modelNewsImage = R.drawable.bank_ic_payment_activity_image2
        mNewsArray.add(model2)

        val model3 = BankModel()
        model3.modelNewsTitle = resources.getString(R.string.bank_newsCardTitle1)
        model3.modelNewsImage = R.drawable.bank_ic_payment_activity_image3
        mNewsArray.add(model3)

        return mNewsArray
    }


}
