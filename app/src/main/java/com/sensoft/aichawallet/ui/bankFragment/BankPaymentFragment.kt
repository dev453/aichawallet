package com.sensoft.aichawallet.ui.bankFragment


import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.recyclerview.widget.GridLayoutManager

import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_payment.*
import kotlinx.android.synthetic.main.bank_item_payment.view.*

class BankPaymentFragment : BankBaseFragment() {

    val mPaymentArray: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View = inflater.inflate(R.layout.bank_fragment_payment, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val paymentAdapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_payment,
            onBind = { view, _, position ->

                view.imgGridImage.setImageResource(mPaymentArray[position].modelPaymentImage)
                view.txtServiceName.text = mPaymentArray[position].modelPaymentServiceName
            },
            onViewClicked = { _, _, position ->

                val mBankPaymentItemFragment = BankPaymentItemFragment()
                val name = mPaymentArray[position].modelPaymentServiceName
                val bundle = Bundle()
                bundle.putString("title", name)
                mBankPaymentItemFragment.arguments = bundle
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.frameLayout_Container, mBankPaymentItemFragment)?.commit()
            })

        paymentRecyclerview.adapter = paymentAdapter
        paymentRecyclerview.layoutManager =
            GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false)
        paymentAdapter.addItems(paymentData())

        Log.d("test", paymentAdapter.itemCount.toString() + paymentData())
        setStatusBar()
    }

    private fun paymentData(): ArrayList<BankModel> {

        mPaymentArray.clear()
        val model1 = BankModel()
        model1.modelPaymentImage = R.drawable.bank_ic_plug
        model1.modelPaymentServiceName = "Payment Electricity"
        mPaymentArray.add(model1)

        val model2 = BankModel()
        model2.modelPaymentImage = R.drawable.bank_ic_water
        model2.modelPaymentServiceName = "Payment water"
        mPaymentArray.add(model2)

        val model3 = BankModel()
        model3.modelPaymentImage = R.drawable.bank_ic_internet
        model3.modelPaymentServiceName = "Payment Internet"
        mPaymentArray.add(model3)

        val model4 = BankModel()
        model4.modelPaymentImage = R.drawable.bank_ic_phone
        model4.modelPaymentServiceName = "Mobile Prepaid"
        mPaymentArray.add(model4)

        val model5 = BankModel()
        model5.modelPaymentImage = R.drawable.bank_ic_playstore
        model5.modelPaymentServiceName = "Pay Google Play"
        mPaymentArray.add(model5)

        val model6 = BankModel()
        model6.modelPaymentImage = R.drawable.bank_ic_apple
        model6.modelPaymentServiceName = "Pay IOS Store"
        mPaymentArray.add(model6)

        val model7 = BankModel()
        model7.modelPaymentImage = R.drawable.bank_ic_lottery
        model7.modelPaymentServiceName = "Buy Lottery tickets"
        mPaymentArray.add(model7)

        val model8 = BankModel()
        model8.modelPaymentImage = R.drawable.bank_ic_train
        model8.modelPaymentServiceName = "Buy train tickets"
        mPaymentArray.add(model8)

        val model9 = BankModel()
        model9.modelPaymentImage = R.drawable.bank_ic_airticket
        model9.modelPaymentServiceName = "Buy air ticket"
        mPaymentArray.add(model9)

        val model10 = BankModel()
        model10.modelPaymentImage = R.drawable.bank_ic_shopping
        model10.modelPaymentServiceName = "Shopping Online"
        mPaymentArray.add(model10)

        val model11 = BankModel()
        model11.modelPaymentImage = R.drawable.bank_ic_hotel
        model11.modelPaymentServiceName = "Booking Hotel"
        mPaymentArray.add(model11)

        return mPaymentArray
    }

    private fun setStatusBar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorWhitePure)
                ?.let { window.statusBarColor = it }
        }
    }
}
