package com.sensoft.aichawallet.ui.authentification.password_manager

import android.content.Intent
import android.os.Bundle
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.R
import kotlinx.android.synthetic.main.bank_activity_resend.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class ResendActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bank_activity_resend)

        txtMenuItemTitle.text = resources.getString(R.string.bank_lbl_txtForgetPasswordTitle)


        imgMenuBackImageView.setOnClickListener {
            onBackPressed()
        }

        btnNext_ResendActivity.setOnClickListener {
            startActivity(Intent(this, CreatePasswordActivity::class.java))
        }
    }
}
