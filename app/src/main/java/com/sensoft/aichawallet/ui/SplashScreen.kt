package com.sensoft.aichawallet.ui

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.WindowManager
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.ui.authentification.sign_up.ActivationRequestActivity

class SplashScreen : BaseActivity() {

    private val SPLASHTIMEOUT: Long = 1000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        Handler().postDelayed({
            startActivity(Intent(this, ActivationRequestActivity::class.java))
            finish()
        }, SPLASHTIMEOUT)

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )
    }
}
