package com.sensoft.aichawallet.ui.bankFragment

import android.graphics.drawable.Drawable
import android.os.Build
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankUserAccountAdapter
import kotlinx.android.synthetic.main.bank_fragment_home.*

@Suppress("UNREACHABLE_CODE", "DEPRECATION")
class BankHomeFragment : BankBaseFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.bank_fragment_home, container, false
    )

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setStatusBar()

        val userAccountAdapter = BankUserAccountAdapter(context)
        vpUser?.adapter = userAccountAdapter

        val mDotsCount = userAccountAdapter.count
        val mDot = arrayOfNulls<ImageView>(mDotsCount)

        for (i in 0 until mDotsCount) {
            mDot[0]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_active_dot
                )
            })
            mDot[i] = ImageView(context)
            mDot[i]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_non_active_dot
                )
            })
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            dotIndicator.addView(mDot[i], params)
        }

        vpUser?.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until mDotsCount) {
                    mDot[i]?.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.bank_non_active_dot
                        )
                    })
                }
                mDot[position]?.setImageDrawable(context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.bank_active_dot
                    )
                })
            }
        })
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun setStatusBar() {
        val window: Window? = activity?.window
        val drawable: Drawable? = activity?.resources?.getDrawable(R.drawable.bank_gradientbackground)
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(android.R.color.transparent)
                ?.let { window.statusBarColor = it }
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
        }
        window?.setBackgroundDrawable(drawable);
    }
}
