package com.sensoft.aichawallet.ui.authentification

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationAccount
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import com.sensoft.aichawallet.core.BaseViewModel
import com.sensoft.aichawallet.models.activation.account.ActivationAccountData
import com.sensoft.aichawallet.models.activation.account.ActivationAccountRepository
import com.sensoft.aichawallet.models.activation.request.ActivationData
import com.sensoft.aichawallet.models.activation.request.ActivationRepository
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class ActivationAccountViewModel @Inject constructor(val context: Context, private val activationAccountRepository: ActivationAccountRepository) :
    BaseViewModel() {
    private val liveResponseAccountActivation = MutableLiveData<ResponseActivationAccount>()
    val error = MutableLiveData<String>()

    fun onActivationDone():LiveData<ResponseActivationAccount>{
        return liveResponseAccountActivation
    }

    fun onErrorInActivation():LiveData<String>{
        return error
    }

    fun activateRequest(activationData: ActivationAccountData){
        activationAccountRepository.run {
            activateRequest(activationData).subscribeBy(
                onError = {
                    error.postValue(it.localizedMessage)
                },
                onSuccess = {
                    liveResponseAccountActivation.value=it
                }
            )
        }
    }

    companion object {
        private const val TAG = "ActivationAccountViewMo"
    }
}