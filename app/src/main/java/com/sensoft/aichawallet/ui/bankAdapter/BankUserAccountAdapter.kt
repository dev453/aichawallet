package com.sensoft.aichawallet.ui.bankAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import kotlinx.android.synthetic.main.bank_layout_userdetails.view.*

class BankUserAccountAdapter(var context: Context?) : PagerAdapter() {

    var Inflator: LayoutInflater? = null
    var UserAccountName = context?.resources?.getStringArray(R.array.bank_no_of_account)
    var AccountNumber = context?.resources?.getStringArray(R.array.bank_account_number)
    var AccountBalance = context?.resources?.getStringArray(R.array.bank_user_balance)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = 3

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        if (context != null) {
            Inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        val view = Inflator!!.inflate(R.layout.bank_layout_userdetails, null)

        view.lbl_txtDefaultUser.text = UserAccountName?.get(position)
        view.txtAccountNumber.text = AccountNumber?.get(position)
        view.txtUserBalance.text = AccountBalance?.get(position)

        val mViewPager = container as ViewPager
        mViewPager.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val mViewPager = container as ViewPager
        val mView = `object` as View
        mViewPager.removeView(mView)
    }

}