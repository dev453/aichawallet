package com.sensoft.aichawallet.ui.bankFragment


import android.os.Bundle
import android.view.*
import androidx.recyclerview.widget.LinearLayoutManager

import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_saving.*
import kotlinx.android.synthetic.main.bank_item_savingonline.view.*

class BankSavingFragment : BankBaseFragment() {

    var mSavingArray: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_saving, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        addNewSaving.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankAddNewSavingFragment())?.commit()
        }

        val savingAdapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_savingonline,
            onBind = { view, item, position ->
                view.txtSavingName.text = mSavingArray[position].modelSavingName
                view.txtSavingAmount.text = mSavingArray[position].modelSavingAmount
                view.txtSavingInterest.text = mSavingArray[position].modelSavingInterest
                view.txtSavingDate.text = mSavingArray[position].modelSavingDate
            },
            onViewClicked = { view, item, position -> })

        savingOnlineRecyclerview.adapter = savingAdapter
        savingOnlineRecyclerview.layoutManager = LinearLayoutManager(context)
        savingAdapter.addItems(addSavingItem())

        setStatusBar()
    }

    private fun addSavingItem(): ArrayList<BankModel> {

        mSavingArray.clear()

        val model1 = BankModel()
        model1.modelSavingName = "Saving 1"
        model1.modelSavingAmount = "$12,000"
        model1.modelSavingInterest = "Interest 8% - 8 Months"
        model1.modelSavingDate = resources.getString(R.string.bank_22_apr_2020)
        mSavingArray.add(model1)

        val model2 = BankModel()
        model2.modelSavingName = "Saving 2"
        model2.modelSavingAmount = "$20,000"
        model2.modelSavingInterest = "Interest 4% - 2 Months"
        model2.modelSavingDate = resources.getString(R.string.bank_22_mar_2020)
        mSavingArray.add(model2)

        return mSavingArray
    }

    private fun setStatusBar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorBackgroundFragment)
                ?.let { window.statusBarColor = it }
        }
    }
}
