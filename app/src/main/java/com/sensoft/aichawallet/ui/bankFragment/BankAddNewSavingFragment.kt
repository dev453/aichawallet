package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankUserCardAdapter
import kotlinx.android.synthetic.main.bank_fragment_addnewsaving.*

class BankAddNewSavingFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_addnewsaving, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        imgMenuBackImageView.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankSavingFragment())?.commit()
        }

        btnAddNewSavingConfirm1.setOnClickListener {
            btnAddNewSavingConfirm1.visibility = View.GONE
            lbl_txtAddNewSavingSubTitle2.visibility = View.VISIBLE
            edtAddNewSavingOTP.visibility = View.VISIBLE
            txtAddNewSavingResendOTP.visibility = View.VISIBLE
            lbl_txtAddNewSavingSubTitle3.visibility = View.VISIBLE
            imgAddNewSavingFaceId.visibility = View.VISIBLE
            btnAddNewSavingConfirm2.visibility = View.VISIBLE
        }

        btnAddNewSavingConfirm2.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankSavingSuccessfullfragment())?.commit()
        }

        val bankUserCardAdapter = BankUserCardAdapter(context)
        vpAddNewSaving.adapter = bankUserCardAdapter

        val mDotsCount = bankUserCardAdapter.count
        val mDot = arrayOfNulls<ImageView>(mDotsCount)

        for (i in 0 until mDotsCount) {
            mDot[0]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_active_dot
                )
            })
            mDot[i] = ImageView(context)
            mDot[i]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_non_active_dot
                )
            })
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            dotIndicator.addView(mDot[i], params)
        }

        vpAddNewSaving.clipToPadding = false
        vpAddNewSaving.setPadding(resources.getDimension(R.dimen._12sdp).toInt(), 0, resources.getDimension(R.dimen._40sdp).toInt(), 0)
        vpAddNewSaving.pageMargin = 40

        vpAddNewSaving.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until mDotsCount) {
                    mDot[i]?.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.bank_non_active_dot
                        )
                    })
                }
                mDot[position]?.setImageDrawable(context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.bank_active_dot
                    )
                })
            }
        })
    }

}
