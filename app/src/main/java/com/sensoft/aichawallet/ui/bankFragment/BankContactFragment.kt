package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankContactFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_contact, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_contact)
    }
}
