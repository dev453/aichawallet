package com.sensoft.aichawallet.ui.bankFragment


import android.annotation.SuppressLint
import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.view.*
import androidx.annotation.RequiresApi
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_fragment_menu.*


class BankMenuFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_menu, container, false)

    @RequiresApi(Build.VERSION_CODES.P)
    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        txtMenuChangePassword.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankChangePasswordFragment())?.commit()
        }

        txtMenuShareInformation.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankShareInformationFragment())?.commit()
        }

        txtMenuNews.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankNewsFragment())?.commit()
        }

        txtMenuRate.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankRateInformationFragment())?.commit()
        }

        txtMenuLocation.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankLocationFragment())?.commit()
        }

        txtMenuTerms.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankTermsConditionFragment())?.commit()
        }

        txtMenuTerms.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankTermsConditionFragment())?.commit()
        }

        txtMenuContacts.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankContactFragment())?.commit()
        }

        txtMenuQuestions.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankQuestionFragment())?.commit()
        }

        txtMenuLogout.setOnClickListener {
            val dialogBuilder = Dialog(context!!)
            val dialogLayoutInflater = layoutInflater.inflate(R.layout.bank_layout_logoutdialog, null)
            dialogBuilder.window?.decorView?.setBackgroundDrawable(ColorDrawable(android.R.color.transparent))
            dialogBuilder.setContentView(dialogLayoutInflater)
            dialogBuilder.setCancelable(true)
            dialogBuilder.show()
        }

        setStatusbar()
    }

    private fun setStatusbar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorBackgroundFragment)
                ?.let { window.statusBarColor = it }
        }
    }

}
