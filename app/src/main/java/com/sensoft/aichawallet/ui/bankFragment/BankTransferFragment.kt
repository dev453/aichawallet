package com.sensoft.aichawallet.ui.bankFragment


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.*
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankUserCardAdapter
import kotlinx.android.synthetic.main.bank_fragment_transfer.*


@Suppress("DEPRECATION")
class BankTransferFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(
        R.layout.bank_fragment_transfer, container, false
    )

    @SuppressLint("ResourceAsColor")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnToggle.setOnCheckedChangeListener { _, isChecked ->
            if (isChecked) {
                btnGetOTP.visibility = View.VISIBLE
                lbl_txtLine10.visibility = View.GONE
                lbl_txtLine10.visibility = View.GONE
                txtDropDown.visibility = View.GONE
                lbl_txtTransferVia.visibility = View.GONE
                lbl_txtLine.visibility = View.GONE
                vpCardDetails.visibility = View.INVISIBLE
                imgCardImage.visibility = View.VISIBLE
                lbl_txtChooseTransaction.visibility = View.GONE
                dotIndicator.visibility = View.GONE
                txtTransferTitle.text = resources.getString(R.string.bank_confirm_transfer)
                lbl_txtChooseAccount.text = resources.getString(R.string.bank_transfer_from)
                scrollView.scrollTo(0, 100)
            }
        }

        btnGetOTP.setOnClickListener {
            lbl_transferTo9.visibility = View.GONE
            btnToggle.visibility = View.GONE
            lbl_txtOTPText.visibility = View.VISIBLE
            edtOTP.visibility = View.VISIBLE
            txtResendOtp.visibility = View.VISIBLE
            lbl_txtUseFace.visibility = View.VISIBLE
            imgFaceId.visibility = View.VISIBLE
            btnConfirm.visibility = View.VISIBLE
            btnGetOTP.visibility = View.GONE
            btnConfirm.visibility = View.VISIBLE
        }

        btnConfirm.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankTransferSuccessfulFragment())?.commit()
        }

        lbl_txt1.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankBeneficiaryListFragment())?.commit()
        }

        val bankUserCardAdapter = BankUserCardAdapter(context)
        vpCardDetails.adapter = bankUserCardAdapter

        val mDotsCount = bankUserCardAdapter.count
        val mDot = arrayOfNulls<ImageView>(mDotsCount)

        for (i in 0 until mDotsCount) {
            mDot[0]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_active_dot
                )
            })
            mDot[i] = ImageView(context)
            mDot[i]?.setImageDrawable(context?.let {
                ContextCompat.getDrawable(
                    it,
                    R.drawable.bank_non_active_dot
                )
            })
            val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
            )
            params.setMargins(8, 0, 8, 0)
            dotIndicator.addView(mDot[i], params)
        }

        vpCardDetails.clipToPadding = false
        vpCardDetails.setPadding(resources.getDimension(R.dimen._12sdp).toInt(), 0, resources.getDimension(R.dimen._40sdp).toInt(), 0)
        vpCardDetails.pageMargin = 40

        vpCardDetails.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until mDotsCount) {
                    mDot[i]?.setImageDrawable(context?.let {
                        ContextCompat.getDrawable(
                            it,
                            R.drawable.bank_non_active_dot
                        )
                    })
                }
                mDot[position]?.setImageDrawable(context?.let {
                    ContextCompat.getDrawable(
                        it,
                        R.drawable.bank_active_dot
                    )
                })
            }
        })
        txtDropDown.setOnClickListener {
            val dialogBuilder = AlertDialog.Builder(context)
            val dialog = layoutInflater.inflate(R.layout.bank_layout_dialogtransfer, null)
            dialogBuilder.setView(dialog)
            dialogBuilder.setCancelable(true)
            val alert = dialogBuilder.create()
            alert.window?.setBackgroundDrawable(ColorDrawable(android.R.color.transparent))
            val params: WindowManager.LayoutParams? = alert.window?.attributes
            if (params != null) {
                params.width = resources.getDimension(R.dimen._270sdp).toInt()
                params.height = resources.getDimension(R.dimen._400sdp).toInt()
            }
            alert.window?.attributes = params
            alert.show()
        }

        setStatusBar()
    }

    private fun setStatusBar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorBackgroundFragment)
                ?.let { window.statusBarColor = it }
        }
    }
}
