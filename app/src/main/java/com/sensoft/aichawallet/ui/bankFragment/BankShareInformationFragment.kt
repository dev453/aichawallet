package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_shareinformation.*
import kotlinx.android.synthetic.main.bank_item_shareinformation.view.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*
import java.util.*


class BankShareInformationFragment : BankBaseFragment() {

    var mShareInfoArray: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_shareinformation, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_share_informationtitle)


        val shareInfoAdapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_shareinformation,
            onBind = { bindView, item, position ->
                bindView.imgCardImageView.setImageResource(mShareInfoArray[position].modelShareInfoImages)
            },
            onViewClicked = { _, _, _ -> })

        shareInfoRecyclerview.adapter = shareInfoAdapter
        shareInfoRecyclerview.layoutManager =
            LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        shareInfoAdapter.addItems(addImages())

        shareInfoRecyclerview.clipToPadding = false
        shareInfoRecyclerview.setPadding(30, 0, 0, 0)
    }

    private fun addImages(): ArrayList<BankModel> {

        val model1 = BankModel()
        model1.modelShareInfoImages = R.drawable.bank_ic_skype
        mShareInfoArray.add(model1)

        val model2 = BankModel()
        model2.modelShareInfoImages = R.drawable.bank_ic_instagram
        mShareInfoArray.add(model2)

        val model3 = BankModel()
        model3.modelShareInfoImages = R.drawable.bank_ic_whatsapp
        mShareInfoArray.add(model3)

        val model4 = BankModel()
        model4.modelShareInfoImages = R.drawable.bank_ic_messenger
        mShareInfoArray.add(model4)

        val model5 = BankModel()
        model5.modelShareInfoImages = R.drawable.bank_ic_facebook
        mShareInfoArray.add(model5)

        return mShareInfoArray
    }
}
