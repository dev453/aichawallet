package com.sensoft.aichawallet.ui.authentification.di

import com.sensoft.aichawallet.di.GlobalInjectorModule
import com.sensoft.aichawallet.di.ViewModelModule
import com.sensoft.aichawallet.ui.authentification.sign_up.ActicationAccountActivity
import com.sensoft.aichawallet.ui.authentification.sign_up.ActivationRequestActivity
import dagger.Component


@Component(modules = [GlobalInjectorModule::class, ViewModelModule::class])
interface ActivationAccountComponent {
    fun inject(activationAccountActivity: ActicationAccountActivity)
}