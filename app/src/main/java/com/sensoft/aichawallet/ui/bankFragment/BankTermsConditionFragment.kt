package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_title_item_layout.*


class BankTermsConditionFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_terms_condition, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        setStatusbar()

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_terms_conditions)
    }

    private fun setStatusbar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorWhitePure)
                ?.let { window.statusBarColor = it }
        }
    }

}
