package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import android.widget.ImageView
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_fragment_paymentinvoice.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankPaymentInvoiceFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ):
            View = inflater.inflate(R.layout.bank_fragment_paymentinvoice, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankPaymentItemFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_pay_invoice)

        btnInvoiceNext.setOnClickListener {
            val mBankInvoiceFragment = BankInvoiceFragment()
            val getCode = edtInvitationCode.text.toString()

            val bundle = Bundle()
            bundle.putString("code", getCode)
            mBankInvoiceFragment.arguments = bundle

            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, mBankInvoiceFragment)?.commit()
        }
        setStatusbar()
    }
    private fun setStatusbar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorWhitePure)
                ?.let { window.statusBarColor = it }
        }
    }
}
