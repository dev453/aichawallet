package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_location.*
import kotlinx.android.synthetic.main.bank_item_location.view.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*


class BankLocationFragment : BankBaseFragment() {

    val mLocationArray: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_location, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_branch_location)

        val locationAdapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_location,
            onBind = { view, _, position ->

                view.txtLocationName.text = mLocationArray[position].modelLocationName
                view.txtDistance.text = mLocationArray[position].modelLocationDistance.toString()

            },
            onViewClicked = { _, _, _ ->
                activity?.supportFragmentManager?.beginTransaction()
                    ?.replace(R.id.frameLayout_Container, BankBranchFragment())?.commit()
            })

        locationRecyclerview.adapter = locationAdapter
        locationRecyclerview.layoutManager = LinearLayoutManager(context)
        locationAdapter.addItems(addLocationDetails())
    }

    private fun addLocationDetails(): ArrayList<BankModel> {

        val model1 = BankModel()
        model1.modelLocationName = resources.getString(R.string.bank_branch_canyon)
        model1.modelLocationDistance = resources.getString(R.string.bank_800)
        mLocationArray.add(model1)

        val model2 = BankModel()
        model2.modelLocationName = resources.getString(R.string.bank_branch_kenmore)
        model2.modelLocationDistance = resources.getString(R.string.bank_600)
        mLocationArray.add(model2)

        val model3 = BankModel()
        model3.modelLocationName = resources.getString(R.string.bank_branch_woodinville)
        model3.modelLocationDistance = resources.getString(R.string.bank_600)
        mLocationArray.add(model3)

        val model4 = BankModel()
        model4.modelLocationName = resources.getString(R.string.bank_branch_maltli)
        model4.modelLocationDistance = resources.getString(R.string.bank_2km)
        mLocationArray.add(model4)

        return mLocationArray
    }
}
