package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView

import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_fragment_changepassword.*
import kotlinx.android.synthetic.main.bank_fragment_changepassword.view.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankChangePasswordFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_changepassword, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_change_password_title)

        view.imgOld?.setOnClickListener {

            if (edtOldPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                imgOld.setImageResource(R.drawable.bank_ic_visibility_black_24dp)
                edtOldPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance();
            } else {
                imgOld.setImageResource(R.drawable.bank_ic_visibility_off_black_24dp)
                edtOldPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

        view.imgNew?.setOnClickListener {

            if (edtNewPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                imgNew.setImageResource(R.drawable.bank_ic_visibility_black_24dp)
                edtNewPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance();
            } else {
                imgNew.setImageResource(R.drawable.bank_ic_visibility_off_black_24dp)
                edtNewPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }

        view.imgConfirm?.setOnClickListener {

            if (edtConfirmPassword.transformationMethod == PasswordTransformationMethod.getInstance()) {
                imgConfirm.setImageResource(R.drawable.bank_ic_visibility_black_24dp)
                edtConfirmPassword.transformationMethod =
                    HideReturnsTransformationMethod.getInstance();
            } else {
                imgConfirm.setImageResource(R.drawable.bank_ic_visibility_off_black_24dp)
                edtConfirmPassword.transformationMethod =
                    PasswordTransformationMethod.getInstance()
            }
        }
    }

}
