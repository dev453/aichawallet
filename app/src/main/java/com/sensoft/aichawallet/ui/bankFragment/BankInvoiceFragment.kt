package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.*
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import kotlinx.android.synthetic.main.bank_fragment_invoice.*


class BankInvoiceFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ):
            View = inflater.inflate(R.layout.bank_fragment_invoice, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        val bundle = arguments
        if (bundle != null) {
            val code = bundle.get("code")
            invoiceTitle.text = "Payment Invoice #$code"
            txtCode.text = "#$code"
        }

        btnInvoiceConfirm.setOnClickListener {
            val mBankPayInvoiceFragment = BankPayInvoiceFragment()
            val getcode = invoiceTitle.text.toString()
            val paybundle = Bundle()
            paybundle.putString("Paycode", getcode)
            mBankPayInvoiceFragment.arguments = paybundle

            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankPayInvoiceFragment())?.commit()
        }

        invoiceBack.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()
                ?.replace(R.id.frameLayout_Container, BankPaymentInvoiceFragment())?.commit()
        }

        setStatusbar()
    }

    private fun setStatusbar() {
        val window: Window? = activity?.window
        window?.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
        if (window != null) {
            activity?.resources?.getColor(R.color.bank_colorWhitePure)
                ?.let { window.statusBarColor = it }
        }
    }
}
