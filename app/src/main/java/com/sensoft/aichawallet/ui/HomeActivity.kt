package com.sensoft.aichawallet.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.fragment.app.Fragment
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.ui.bankFragment.*
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : BaseActivity() {

    val mHomeFragment = BankHomeFragment()
    val mTransferFragment = BankTransferFragment()
    val mPaymentFragment = BankPaymentFragment()
    val mSavingFragment = BankSavingFragment()
    val mMenufragment = BankMenuFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_home)

        supportActionBar?.hide()

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout_Container, mHomeFragment).commit()
        }

        bottomNavigationView.setOnNavigationItemSelectedListener { menuItem: MenuItem ->
            return@setOnNavigationItemSelectedListener when (menuItem.itemId) {
                R.id.homeMenu -> {
                    loadFragment(mHomeFragment)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.transferMenu -> {
                    loadFragment(mTransferFragment)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.paymentMenu -> {
                    loadFragment(mPaymentFragment)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.savingMenu -> {
                    loadFragment(mSavingFragment)
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.userMenu -> {
                    loadFragment(mMenufragment)
                    return@setOnNavigationItemSelectedListener true
                }
                else -> false
            }
        }
    }

    fun loadFragment(fragment: Fragment) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frameLayout_Container, fragment)
        transaction.commit()
    }
}
