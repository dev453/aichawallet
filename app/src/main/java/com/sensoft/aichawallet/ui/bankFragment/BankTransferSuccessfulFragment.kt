package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment


class BankTransferSuccessfulFragment : BankBaseFragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.bank_fragment_transfersuccessful, container, false)
    }

}
