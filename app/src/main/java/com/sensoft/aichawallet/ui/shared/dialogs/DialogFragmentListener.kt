

package com.sensoft.aichawallet.ui.shared.dialogs

import androidx.fragment.app.DialogFragment

interface DialogFragmentListener {
    fun onDoneClicked(dialogDoneCancelFragment: DialogFragment)
}