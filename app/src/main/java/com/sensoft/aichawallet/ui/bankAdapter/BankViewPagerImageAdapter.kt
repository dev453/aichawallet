package com.sensoft.aichawallet.ui.bankAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import kotlinx.android.synthetic.main.bank_layout_vpimageslider.view.*

class BankViewPagerImageAdapter(var context: Context, var mImage: Array<Int>) : PagerAdapter() {

    private var mInflater: LayoutInflater? = null
    var getArray = context.resources.getStringArray(R.array.bank_vp_ItemTitle)
    var mTitle = getArray

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = mImage.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        mInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = mInflater!!.inflate(R.layout.bank_layout_vpimageslider, null)

        view.item_vpImage.setImageResource(mImage[position])
        view.item_vpTitleText.text = mTitle[position]

        val mViewPager = container as ViewPager
        mViewPager.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val mViewPager = container as ViewPager
        val mView = `object` as View
        mViewPager.removeView(mView)
    }
}
