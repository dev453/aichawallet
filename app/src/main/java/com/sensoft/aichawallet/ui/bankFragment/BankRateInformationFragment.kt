package com.sensoft.aichawallet.ui.bankFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.LinearLayoutManager
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BankBaseFragment
import com.sensoft.aichawallet.ui.bankAdapter.BankRecyclerViewGenericAdapter
import com.sensoft.aichawallet.models.BankModel
import kotlinx.android.synthetic.main.bank_fragment_rateinformation.*
import kotlinx.android.synthetic.main.bank_item_rateinformation.view.*
import kotlinx.android.synthetic.main.bank_title_item_layout.*

class BankRateInformationFragment : BankBaseFragment() {

    val mRateArray: ArrayList<BankModel> = ArrayList()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ):
            View? = inflater.inflate(R.layout.bank_fragment_rateinformation, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        activity?.findViewById<ImageView?>(R.id.imgMenuBackImageView)?.setOnClickListener {
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.frameLayout_Container,BankMenuFragment())?.commit()
        }

        txtMenuItemTitle.text = resources.getString(R.string.bank_rate_information_title)

        val rateAdapter = BankRecyclerViewGenericAdapter(
            R.layout.bank_item_rateinformation,
            onBind = { view, item, position ->
                view.rateFlag.setImageResource(mRateArray[position].modelRateFlag)
                view.rateCurrency.text = mRateArray[position].modelRateCurrency
                view.rateSell.text = mRateArray[position].modelRateSell.toString()
                view.rateBuy.text = mRateArray[position].modelRateBuy.toString()
            },
            onViewClicked = { view, item, position -> })

        rateInformationRecyclerview.adapter = rateAdapter
        rateInformationRecyclerview.layoutManager = LinearLayoutManager(context)
        rateAdapter.addItems(addRateItem())

    }

    private fun addRateItem(): ArrayList<BankModel> {

        val model1 = BankModel()
        model1.modelRateFlag = R.drawable.bank_ic_europe
        model1.modelRateCurrency = "Euro"
        model1.modelRateBuy = 1.123
        model1.modelRateSell = 3.799
        mRateArray.add(model1)

        val model2 = BankModel()
        model2.modelRateFlag = R.drawable.bank_ic_usflag
        model2.modelRateCurrency = "USD"
        model2.modelRateBuy = 1.123
        model2.modelRateSell = 3.85
        mRateArray.add(model2)

        val model3 = BankModel()
        model3.modelRateFlag = R.drawable.bank_ic_china
        model3.modelRateCurrency = "Rup"
        model3.modelRateBuy = 1.13
        model3.modelRateSell = 2.87
        mRateArray.add(model3)

        val model4 = BankModel()
        model4.modelRateFlag = R.drawable.bank_ic_russia
        model4.modelRateCurrency = "CNY"
        model4.modelRateBuy = 1.0
        model4.modelRateSell = 3.11
        mRateArray.add(model4)

        val model5 = BankModel()
        model5.modelRateFlag = R.drawable.bank_ic_china
        model5.modelRateCurrency = "Won"
        model5.modelRateBuy = 3.0
        model5.modelRateSell = 1.11
        mRateArray.add(model5)

        val model6 = BankModel()
        model6.modelRateFlag = R.drawable.bank_ic_germany
        model6.modelRateCurrency = "Yen"
        model6.modelRateBuy = 1.123
        model6.modelRateSell = 2.0
        mRateArray.add(model6)

        val model7 = BankModel()
        model7.modelRateFlag = R.drawable.bank_ic_canada
        model7.modelRateCurrency = "Euro"
        model7.modelRateBuy = 3.0
        model7.modelRateSell = 1.56
        mRateArray.add(model7)

        val model8 = BankModel()
        model8.modelRateFlag = R.drawable.bank_ic_usflag
        model8.modelRateCurrency = "USD"
        model8.modelRateBuy = 0.1
        model8.modelRateSell = 1.11
        mRateArray.add(model8)


        return mRateArray
    }
}
