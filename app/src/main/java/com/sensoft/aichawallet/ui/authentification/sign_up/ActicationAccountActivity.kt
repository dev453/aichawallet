package com.sensoft.aichawallet.ui.authentification.sign_up

import android.os.Bundle
import android.view.View
import android.widget.EditText
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.di.GlobalInjectorModule
import com.sensoft.aichawallet.helpers.utils.Theme5OTPEditText
import com.sensoft.aichawallet.helpers.utils.launchActivity
import com.sensoft.aichawallet.helpers.utils.onClick
import com.sensoft.aichawallet.ui.authentification.ActivationAccountViewModel
import com.sensoft.aichawallet.ui.authentification.di.DaggerActivationAccountComponent
import com.sensoft.aichawallet.ui.authentification.test.Theme5PasswordSetActivity
import kotlinx.android.synthetic.main.theme5_activity_otp.*
import javax.inject.Inject

class ActicationAccountActivity : BaseActivity() {
    lateinit var viewModel: ActivationAccountViewModel

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private var mEds: Array<EditText?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_actication_account)

        DaggerActivationAccountComponent
            .builder()
            .globalInjectorModule(GlobalInjectorModule(this))
            .build()
            .inject(this)

        viewModel = ViewModelProvider(this, viewModelFactory).get(ActivationAccountViewModel::class.java)

        mEds = arrayOf(edDigit1, edDigit2, edDigit3, edDigit4)
        Theme5OTPEditText(
            mEds!!,
            this
        )
        mEds!!.forEachIndexed { _, editText ->
            editText?.onFocusChangeListener = focusChangeListener
        }
        btnContinue.onClick {
            launchActivity<ActivationPinActivity>()
        }
        ivBack.onClick {
            finish()
        }

        viewModel.onActivationDone().observe(this, Observer {

        })

        viewModel.onErrorInActivation().observe(this, Observer {

        })


    }


    private val focusChangeListener = View.OnFocusChangeListener { v, hasFocus ->

    }
}