package com.sensoft.aichawallet.ui.bankAdapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.R
import kotlinx.android.synthetic.main.bank_layout_transfer.view.*

class BankUserCardAdapter(var context: Context?) : PagerAdapter() {

    var Inflator: LayoutInflater? = null

    var AccountBalance = context?.resources?.getStringArray(R.array.bank_user_balance)
    var bankName = context?.resources?.getStringArray(R.array.bank_spinner_array)
    var name = context?.resources?.getStringArray(R.array.bank_name)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount() = 3

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        if (context != null) {
            Inflator = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        val view = Inflator!!.inflate(R.layout.bank_layout_transfer, null)

        view.card_username.text = name?.get(position)
        view.card_banktitle.text = bankName?.get(position)
        view.card_userbalance.text = AccountBalance?.get(position)

        val mViewPager = container as ViewPager
        mViewPager.addView(view, 0)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val mViewPager = container as ViewPager
        val mView = `object` as View
        mViewPager.removeView(mView)
    }

}