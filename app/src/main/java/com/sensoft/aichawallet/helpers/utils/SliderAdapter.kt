package com.sensoft.aichawallet.helpers.utils

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.sensoft.aichawallet.R
import kotlinx.android.synthetic.main.theem5_item_card.view.*

class SliderAdapter : PagerAdapter() {

        private val mImg = intArrayOf(
            R.drawable.theme5_bg_card_2,
            R.drawable.theme5_bg_card_1,
            R.drawable.theme5_bg_card_3
        )

        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            container.removeView(`object` as View)
        }

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            val view = LayoutInflater.from(container.context)
                .inflate(R.layout.theem5_item_card, container, false)
            view.viewBackground.setBackgroundResource(mImg[position])
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return mImg.size
        }

        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return view === `object` as View
        }
    }
