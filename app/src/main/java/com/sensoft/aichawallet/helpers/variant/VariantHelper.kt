package com.sensoft.aichawallet.helpers.variant

interface VariantHelper {

    fun getBackendEndPoint(): String
}