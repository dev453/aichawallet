package com.sensoft.aichawallet.helpers.extensions

import android.widget.TextView

fun TextView.clear() {
    this.text = ""
}