package com.sensoft.aichawallet

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.ImageView
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.viewpager.widget.ViewPager
import com.sensoft.aichawallet.core.BaseActivity
import com.sensoft.aichawallet.ui.authentification.sign_in.PaymentSigninActivity
import com.sensoft.aichawallet.ui.bankAdapter.BankViewPagerImageAdapter
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        payment_btnGetStarted.setOnClickListener {
            val intent = Intent(
                this,
                PaymentSigninActivity::class.java
            )
            startActivity(intent)
        }

        val mImage = arrayOf(
            R.drawable.bank_ic_payment_activity_image1,
            R.drawable.bank_ic_payment_activity_image2,
            R.drawable.bank_ic_payment_activity_image3
        )

        val viewPagetImageAdapter = BankViewPagerImageAdapter(this, mImage)
        payment_vpSlider.adapter = viewPagetImageAdapter

        val mDotsCount = viewPagetImageAdapter.count

        val mDot = arrayOfNulls<ImageView>(mDotsCount)

        for (i in 0 until mDotsCount) {
            mDot[0]?.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.bank_active_dot))
            mDot[i] = ImageView(this)
            mDot[i]?.setImageDrawable(
                ContextCompat.getDrawable(
                    this,
                    R.drawable.bank_non_active_dot
                )
            )
            val params = LinearLayout.LayoutParams(
                10,
                10
            )
            params.setMargins(8, 0, 8, 0)
            payment_vpSliderDotIndicator.addView(mDot[i], params)
        }
        payment_vpSlider.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(state: Int) {
            }

            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                for (i in 0 until mDotsCount) {
                    mDot[i]?.setImageDrawable(
                        ContextCompat.getDrawable(
                            applicationContext,
                            R.drawable.bank_non_active_dot
                        )
                    )
                }
                mDot[position]?.setImageDrawable(
                    ContextCompat.getDrawable(
                        applicationContext,
                        R.drawable.bank_active_dot
                    )
                )
            }
        })
    }
}
