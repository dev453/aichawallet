
package com.sensoft.aichawallet

class Constants {

    companion object {
        const val DEV_URL = "https://api2.sensoft-labs.com:8243/"
        const val PROD_URL = "https://api2.sensoft-labs.com:8243/"
        const val USER_PREFERENCES_KEY = "USER_PREFERENCES_KEY"
        const val PREFERENCE_KEY = "PREFERENCE_KEY"
        const val USER_REFRESH_TOKEN_PREF_KEY = "USER_REFRESH_TOKEN_PREF_KEY"
        const val USER_TOKEN_PREF_KEY = "USER_TOKEN_PREF_KEY"
        const val USER_TOKEN_KEY = "USER_TOKEN_KEY"
        const val WALLET_PROVIDER = "AICHA"

    }

    enum class HTTPStatus constructor(val code: Int) {
        BAD_REQUEST(400),
        UNAUTHORIZED(401),
        FORBIDDEN(403),
        BAD_EMAIL(422),
        NOT_FOUND(404),
        INTERNAL_SERVER_ERROR(500);

        companion object {
            fun from(findValue: Int): HTTPStatus =
                HTTPStatus.values().first { it.code == findValue }
        }
    }


}