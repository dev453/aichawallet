package com.sensoft.aichawallet.core

import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import com.andrognito.flashbar.Flashbar
import com.orhanobut.logger.Logger
import com.sensoft.aichawallet.AichaAppClass
import com.sensoft.aichawallet.ApiErrorListener
import com.sensoft.aichawallet.R
import com.sensoft.aichawallet.helpers.FlashbarHelper
import com.sensoft.aichawallet.helpers.utils.onClick
import com.sensoft.aichawallet.ui.shared.dialogs.ProgressDialogFragment
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.exceptions.OnErrorNotImplementedException
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.theme5_layout_fingerprint_dialog.*
import org.jetbrains.anko.getStackTraceString
import org.jetbrains.anko.toast
import java.util.concurrent.TimeUnit

open class BaseActivity : AppCompatActivity(), ApiErrorListener {

    private lateinit var progressDialog: ProgressDialogFragment

    private var refreshPageIfNetworkBack: Boolean = false
    private lateinit var flashBarNoNetwork: Flashbar
    lateinit var app: AichaAppClass

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        app = application as AichaAppClass
        app.setInternetConnectionListener(this)
        initializeRxJavaErrorHandler()
        createProgressDialog()
        flashBarNoNetwork = FlashbarHelper.createFlashBarError(
            this,
            getString(R.string.flashbar_becarful_no_network),
            getString(R.string.flashbar_message_no_network)
        )
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase))
    }


    override fun onResume() {
        super.onResume()
        if (!AichaAppClass.isDeviceConnected) {
            // showFlashBarWithDelay()
        }
        if (!(application as AichaAppClass).hasInternetConnectionListener()) {
            (application as AichaAppClass).setInternetConnectionListener(this)
        }
        if (RxJavaPlugins.getErrorHandler() == null) {
            initializeRxJavaErrorHandler()
        }
        if (refreshPageIfNetworkBack) {
            if (AichaAppClass.isDeviceConnected) {
                finish()
                startActivity(intent)
            } else {
                //lostConnectivityDialog.show(this, "lostConnectivityDialog")
            }
            refreshPageIfNetworkBack = false
        }
    }

    override fun onPause() {
        super.onPause()
        if (!AichaAppClass.isDeviceConnected) {
            refreshPageIfNetworkBack = true
        }
        (application as AichaAppClass).setInternetConnectionListener(null)
        RxJavaPlugins.setErrorHandler(null)
    }



    override fun onInternetAvailable() {
        if (flashBarNoNetwork.isShown()) {
            flashBarNoNetwork.dismiss()
        }
    }

    override fun onInternetUnavailable() {
        //  showFlashBarWithDelay()
        toast("no network")
    }

    override fun onUnauthorized() {
        /*
        if (this !is LoginActivity && this !is RegisterActivity) {

            HomeAuthActivity.startActivity(this)
            toast("unauthorized")
        }
        */
    }

    private fun createProgressDialog() {
        progressDialog = ProgressDialogFragment.createDialog()
    }



    private fun initializeRxJavaErrorHandler() {

        RxJavaPlugins.setErrorHandler { e ->
            var throwable = e

            if (throwable is OnErrorNotImplementedException) {
                throwable = throwable.cause
            }
            if (throwable.localizedMessage == null) {
                Logger.e(throwable.getStackTraceString())
            } else {
                Logger.e(throwable.getStackTraceString())
            }

            Observable.timer(250, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy {
                    FlashbarHelper
                        .createFlashBarError(
                            this,
                            getString(R.string.flashbar_becarful_error),
                            getString(R.string.flashbar_message_error)
                        )
                        .show()
                }

            //  Crashlytics.logException(throwable)

        }
    }

    fun showProgressDialog() {
        supportFragmentManager.executePendingTransactions()
        if (!progressDialog.isAdded) {
            progressDialog.show(this, "ProgressDialog")
        }
    }

    fun hideProgressDialog() {
        if (progressDialog.isAdded) {
            progressDialog.dismiss()
        }
    }



    fun isLogged(): Boolean {
        return app.isLogged()
    }

    fun showErrorFlashBar(message: String) {
        FlashbarHelper
            .createFlashBarError(this, getString(R.string.flashbar_becarful_no_network), message)
            .show()
    }

    /* private fun showFlashBarWithDelay() {
        if (!flashBarNoNetwork.isShown() && this !is SplashScreenActivity) {
             Handler().postDelayed({
                 flashBarNoNetwork.show()
             }, 250)
         }
     }*/

    private fun showFingerPrintDialog() {
        var dialog = Dialog(this)
        dialog.window?.setBackgroundDrawable(ColorDrawable(0))
        dialog.setContentView(R.layout.theme5_layout_fingerprint_dialog)
        dialog.window?.setLayout(
            FrameLayout.LayoutParams.MATCH_PARENT,
            FrameLayout.LayoutParams.WRAP_CONTENT
        )
        dialog.ivCloseDialog.onClick {
            dialog.dismiss()
        }
        dialog.show()
    }
}