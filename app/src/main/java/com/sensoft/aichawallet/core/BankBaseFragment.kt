package com.sensoft.aichawallet.core

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment


/**
 * A simple [Fragment] subclass.
 * Use the [BankBaseFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
open class BankBaseFragment : Fragment() {

    lateinit var activity: BaseActivity
        private set

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is BaseActivity) {
            activity = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(false)
    }

}