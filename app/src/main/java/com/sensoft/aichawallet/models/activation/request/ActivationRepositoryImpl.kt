package com.sensoft.aichawallet.models.activation.request

import com.sensoft.aichawallet.api.activation.request.ActivationApiServiceImpl
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class ActivationRepositoryImpl @Inject constructor() :
    ActivationRepository {
    @Inject
    lateinit var actiService: ActivationApiServiceImpl

    override fun activateRequest(activationData: ActivationData): Single<ResponseActivationRequest> = Single.create { emitter->
        actiService.run {
            activateRequest(activationData)
                .subscribeBy(
                    onSuccess = {responseActivationRequest ->
                        emitter.onSuccess(responseActivationRequest)
                    },
                    onError = {
                        emitter.onError(it)
                    }
                )
        }
    }
}