package com.sensoft.aichawallet.models

import java.io.Serializable

class BankModel : Serializable {
    var modelUserImage: Int = 0
    var modelUserName = ""
    var modelUserAccountNumber = ""
    var modelUserBankName = ""

    var modelPaymentImage: Int = 0
    var modelPaymentServiceName = ""

    var modelSavingImage: Int = 0
    var modelSavingName = ""
    var modelSavingAmount = ""
    var modelSavingInterest = ""
    var modelSavingDate = ""

    var modelRateFlag: Int = 0
    var modelRateCurrency = ""
    var modelRateBuy: Double = 0.00
    var modelRateSell: Double = 0.00

    var modelLocationName = ""
    var modelLocationDistance = ""

    var modelQuestion = ""

    var modelShareInfoImages: Int = 0

    var modelNewsTitle = ""
    var modelNewsImage : Int = 0
}