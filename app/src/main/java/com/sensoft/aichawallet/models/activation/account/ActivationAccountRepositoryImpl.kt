package com.sensoft.aichawallet.models.activation.account

import com.sensoft.aichawallet.api.activation.request.ActivationApiServiceImpl
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationAccount
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class ActivationAccountRepositoryImpl @Inject constructor() :
    ActivationAccountRepository {
    @Inject
    lateinit var actiService: ActivationApiServiceImpl

    override fun activateRequest(activationAccountData: ActivationAccountData): Single<ResponseActivationAccount> = Single.create { emitter->
        actiService.run {
            activateRequest(activationAccountData)
                .subscribeBy(
                    onSuccess = {responseActivationRequest ->
                        emitter.onSuccess(responseActivationRequest)
                    },
                    onError = {
                        emitter.onError(it)
                    }
                )
        }
    }
}