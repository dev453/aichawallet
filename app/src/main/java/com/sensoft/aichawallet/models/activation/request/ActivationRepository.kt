package com.sensoft.aichawallet.models.activation.request

import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import io.reactivex.Single

interface ActivationRepository {
    fun activateRequest(activationData: ActivationData): Single<ResponseActivationRequest>

}