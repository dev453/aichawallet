package com.sensoft.aichawallet.models.activation.account

import com.squareup.moshi.Json

data class ActivationAccountData(

	@field:Json(name="walletId")
	val walletId: String? = null,

	@field:Json(name="walletProvider")
	val walletProvider: String? = null
)
