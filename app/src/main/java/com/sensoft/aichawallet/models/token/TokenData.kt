package com.sensoft.aichawallet.models.token

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TokenData(
	val clientId: String? = null,
	val grantType: String? = null,
	val clientSecret: String? = null,
	val password: String? = null,
	val username: String? = null,
	val scope: String? = null
) : Parcelable

data class TokenDto(val accesToken:String,val refreshToken:String,val expireIn:Int,val tokenType:String)
