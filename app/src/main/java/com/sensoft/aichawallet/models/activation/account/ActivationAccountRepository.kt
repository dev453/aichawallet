package com.sensoft.aichawallet.models.activation.account

import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationAccount
import com.sensoft.aichawallet.models.activation.account.ActivationAccountData
import io.reactivex.Single

interface ActivationAccountRepository {
    fun activateRequest(activationAccountData: ActivationAccountData): Single<ResponseActivationAccount>

}