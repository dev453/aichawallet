package com.sensoft.aichawallet.models.activation.request

import com.squareup.moshi.Json

data class ActivationData(

	@field:Json(name="walletId")
	val walletId: String? = null,

	@field:Json(name="walletProvider")
	val walletProvider: String? = null
)
