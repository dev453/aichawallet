package com.sensoft.aichawallet.api.activation.request

import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import com.sensoft.aichawallet.models.activation.request.ActivationData
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ActivationApi {
    @Headers("Content-Type: application/json")
    @POST("aichav4/v1/wallet-activation-request")
    fun activateRequest(@Body activationData: ActivationData): Single<Response<ResponseActivationRequest>>
}