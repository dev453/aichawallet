package com.sensoft.aichawallet.api.activation.request

import com.orhanobut.logger.Logger
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import com.sensoft.aichawallet.models.activation.request.ActivationData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import retrofit2.Retrofit
import javax.inject.Inject

class ActivationApiServiceImpl @Inject constructor(retrofit: Retrofit) :
    ActivationApiService {
    private var activationApi: ActivationApi = retrofit.create(ActivationApi::class.java)

    override fun activateRequest(activationData: ActivationData): Single<ResponseActivationRequest> = Single.create{ emitter->
       doAsync {
           activationApi.activateRequest(activationData)
               .observeOn(AndroidSchedulers.mainThread())
               .subscribeOn(Schedulers.io())
               .subscribeBy(
                   onSuccess = { response ->
                       if (response.isSuccessful) {
                           emitter.onSuccess(response.body() ?: ResponseActivationRequest())
                       } else {
                           emitter.onError(Error())
                       }
                   },
                   onError = {
                       Logger.e(it.localizedMessage)
                       emitter.onError(it)
                   })
       }
    }


}