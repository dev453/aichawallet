package com.sensoft.aichawallet.api.models.responseToken

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.sensoft.aichawallet.models.token.TokenDto
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseGetToken(

	@field:SerializedName("access_token")
	val access_token: String? = null,

	@field:SerializedName("refresh_token")
	val refresh_token: String? = null,

	@field:SerializedName("refresh_expires_in")
	val refresh_expires_in: Int? = null,

	@field:SerializedName("not-before-policy")
	val notBeforePolicy: Int? = null,

	@field:SerializedName("scope")
	val scope: String? = null,

	@field:SerializedName("token_type")
	val token_type: String? = null,

	@field:SerializedName("session_state")
	val session_state: String? = null,

	@field:SerializedName("expires_in")
	val expires_in: Int? = null
) : Parcelable

fun ResponseGetToken.toTokenDto():TokenDto{
	return TokenDto(
		this.accessToken!!,
		this.refreshToken!!,
		this.expiresIn!!,
		this.tokenType!!
	)
}
