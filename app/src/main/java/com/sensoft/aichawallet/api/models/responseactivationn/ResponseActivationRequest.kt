package com.sensoft.aichawallet.api.models.responseactivationn

import com.squareup.moshi.Json

data class ResponseActivationRequest(

	@field:Json(name="activeAccount")
	val activeAccount: Boolean? = null,

	@field:Json(name="lastCashOutDate")
	val lastCashOutDate: Any? = null,

	@field:Json(name="accountType")
	val accountType: String? = null,

	@field:Json(name="accountTypeCode")
	val accountTypeCode: String? = null,

	@field:Json(name="balanceAtTheEndOfTheMonth")
	val balanceAtTheEndOfTheMonth: Double? = null,

	@Json(name="branchName")
	val branchName: String? = null,

	@field:Json(name="customerCode")
	val customerCode: String? = null,

	@field:Json(name="accountTypeName")
	val accountTypeName: String? = null,

	@field:Json(name="accountInOpposition")
	val accountInOpposition: Boolean? = null,

	@field:Json(name="lastCashinDate")
	val lastCashinDate: Any? = null,

	@field:Json(name="accountNumber")
	val accountNumber: String? = null,

	@field:Json(name="blockedAmount")
	val blockedAmount: Int? = null,

	@field:Json(name="balanceOnTheEve")
	val balanceOnTheEve: Double? = null,

	@field:Json(name="customerName")
	val customerName: String? = null,

	@field:Json(name="branchCode")
	val branchCode: String? = null,

	@field:Json(name="balanceAtTheEndOfTheYear")
	val balanceAtTheEndOfTheYear: Double? = null,

	@field:Json(name="balance")
	val balance: Double? = null,

	@field:Json(name="minimumBalance")
	val minimumBalance: Double? = null,

	@field:Json(name="blockedAccount")
	val blockedAccount: Boolean? = null,

	@field:Json(name="accountClosureReason")
	val accountClosureReason: String? = null
)
