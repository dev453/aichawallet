package com.sensoft.aichawallet.api.activation.request

import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationRequest
import com.sensoft.aichawallet.models.activation.request.ActivationData
import io.reactivex.Single

interface ActivationApiService {
    fun activateRequest(activationData: ActivationData): Single<ResponseActivationRequest>
}