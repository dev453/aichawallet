package com.sensoft.aichawallet.api.models.responseactivationn

import android.os.Parcelable
import com.squareup.moshi.Json
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseActivationAccount(

	@Json(name="firstname")
	val firstname: String? = null,

	@Json(name="birthdate")
	val birthdate: String? = null,

	@field:Json(name="address")
	val address: List<AddressItem?>? = null,

	@field:Json(name="gender")
	val gender: String? = null,

	@field:Json(name="accountType")
	val accountType: String? = null,

	@field:Json(name="accountTypeCode")
	val accountTypeCode: String? = null,

	@field:Json(name="activityName")
	val activityName: String? = null,

	@field:Json(name="isMembership")
	val isMembership: Boolean? = null,

	@field:Json(name="branchName")
	val branchName: String? = null,

	@field:Json(name="customerCode")
	val customerCode: String? = null,

	@field:Json(name="accountTypeName")
	val accountTypeName: String? = null,

	@field:Json(name="accountNumber")
	val accountNumber: String? = null,

	@field:Json(name="creationDate")
	val creationDate: String? = null,

	@field:Json(name="subActivityName")
	val subActivityName: String? = null,

	@field:Json(name="lastname")
	val lastname: String? = null,

	@field:Json(name="branchCode")
	val branchCode: String? = null,

	@field:Json(name="activityCode")
	val activityCode: String? = null,

	@field:Json(name="subscriptionDate")
	val subscriptionDate: String? = null,

	@field:Json(name="clientType")
	val clientType: String? = null,

	@field:Json(name="subActivityCode")
	val subActivityCode: String? = null,

	@field:Json(name="balance")
	val balance: Double? = null,

	@field:Json(name="phone")
	val phone: String? = null,

	@field:Json(name="isCorporation")
	val isCorporation: Boolean? = null,

	@field:Json(name="email")
	val email: String? = null
) : Parcelable

@Parcelize
data class AddressItem(

	@field:Json(name="city")
	val city: String? = null,

	@field:Json(name="countryCode")
	val countryCode: String? = null,

	@field:Json(name="countryName")
	val countryName: String? = null,

	@field:Json(name="addressLine")
	val addressLine: String? = null
) : Parcelable
