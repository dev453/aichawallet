package com.sensoft.aichawallet.api.activation.account

import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationAccount
import com.sensoft.aichawallet.models.activation.account.ActivationAccountData
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.Headers
import retrofit2.http.POST

interface ActivationAccountApi {
    @Headers("Content-Type: application/json")
    @POST("aichav4/v1/wallet-activation-request")
    fun activateAccountRequest(@Body activationAccountData: ActivationAccountData): Single<Response<ResponseActivationAccount>>
}