package com.sensoft.aichawallet.api.token

import com.sensoft.aichawallet.api.models.responseToken.ResponseGetToken
import retrofit2.Call
import retrofit2.http.*

interface TokenApi {
    @Headers("Content-Type: application/x-www-form-urlencoded")
    @POST("auth/realms/aicha/protocol/openid-connect/token")
    @FormUrlEncoded
    fun token(@Field("grant_type") grant_type: String,
              @Field("client_id") client_id: String,
              @Field("client_secret") client_secret: String,
              @Field("password") password: String,
              @Field("username") username: String,
              @Field("scope") scope: String): Call<ResponseGetToken>
}