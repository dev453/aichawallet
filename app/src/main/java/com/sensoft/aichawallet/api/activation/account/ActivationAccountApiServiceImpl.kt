package com.sensoft.aichawallet.api.activation.account

import com.orhanobut.logger.Logger
import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationAccount
import com.sensoft.aichawallet.models.activation.account.ActivationAccountData
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import retrofit2.Retrofit
import javax.inject.Inject

class ActivationAccountApiServiceImpl @Inject constructor(retrofit: Retrofit) :
    ActivationAccountApiService {
    private var activationAccountApi: ActivationAccountApi = retrofit.create(
        ActivationAccountApi::class.java)

    override fun activateAccountRequest(activationAccountData: ActivationAccountData): Single<ResponseActivationAccount> = Single.create{ emitter->
       doAsync {
           activationAccountApi.activateAccountRequest(activationAccountData)
               .observeOn(AndroidSchedulers.mainThread())
               .subscribeOn(Schedulers.io())
               .subscribeBy(
                   onSuccess = { response ->
                       if (response.isSuccessful) {
                           emitter.onSuccess(response.body() ?: ResponseActivationAccount())
                       } else {
                           emitter.onError(Error())
                       }
                   },
                   onError = {
                       Logger.e(it.localizedMessage)
                       emitter.onError(it)
                   })
       }
    }

}