package com.sensoft.aichawallet.api.activation.account

import com.sensoft.aichawallet.api.models.responseactivationn.ResponseActivationAccount
import com.sensoft.aichawallet.models.activation.account.ActivationAccountData
import io.reactivex.Single

interface ActivationAccountApiService {
    fun activateAccountRequest(activationAccountData: ActivationAccountData): Single<ResponseActivationAccount>
}