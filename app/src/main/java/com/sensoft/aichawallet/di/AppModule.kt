
package com.sensoft.aichawallet.di

import android.content.Context
import com.sensoft.aichawallet.AichaAppClass
import com.sensoft.aichawallet.AppLifecycleObserver
import com.sensoft.aichawallet.token.TokenJWTServiceImpl
import com.sensoft.aichawallet.helpers.sharedpreferences.SharedPreferencesHelper
import com.sensoft.aichawallet.helpers.sharedpreferences.SharedPreferencesHelperImpl
import com.sensoft.aichawallet.token.TokenJWTService
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: AichaAppClass) {

    @Provides
    fun provideApplicationContext(): Context = application

    @Provides
    fun provideAppLifecycleObserver(): AppLifecycleObserver = AppLifecycleObserver(application)

    @Provides
    fun provideSharedPreferencesHelper(): SharedPreferencesHelper = SharedPreferencesHelperImpl()

    @Provides
    fun provideTokenJWTService(service: TokenJWTServiceImpl): TokenJWTService = service

}