/*
 * Copyright (c) 2020 by Appndigital, Inc.
 * All Rights Reserved
 */
package com.sensoft.aichawallet.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.sensoft.aichawallet.ui.authentification.ActivationAccountViewModel
import com.sensoft.aichawallet.ui.authentification.AuthViewModel
import dagger.Binds
import dagger.MapKey
import dagger.Module
import dagger.multibindings.IntoMap
import kotlin.reflect.KClass

@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@MapKey
internal annotation class ViewModelKey(val value: KClass<out ViewModel>)

@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AuthViewModel::class)
    internal abstract fun authViewModel(authViewModel: AuthViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ActivationAccountViewModel::class)
    internal abstract fun activationAccountViewModel(authViewModel: ActivationAccountViewModel): ViewModel

}
