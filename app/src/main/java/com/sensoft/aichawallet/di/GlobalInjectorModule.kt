package com.sensoft.aichawallet.di

import android.app.Activity
import android.content.Context
import android.util.Log
import com.orhanobut.logger.Logger
import com.sensoft.aichawallet.AichaAppClass
import com.sensoft.aichawallet.BuildConfig
import com.sensoft.aichawallet.Constants
import com.sensoft.aichawallet.api.activation.account.ActivationAccountApiService
import com.sensoft.aichawallet.api.activation.account.ActivationAccountApiServiceImpl
import com.sensoft.aichawallet.api.activation.request.ActivationApiService
import com.sensoft.aichawallet.api.activation.request.ActivationApiServiceImpl
import com.sensoft.aichawallet.api.token.TokenApi
import com.sensoft.aichawallet.helpers.network.NetworkConnectionInterceptor
import com.sensoft.aichawallet.helpers.sharedpreferences.SharedPref
import com.sensoft.aichawallet.helpers.sharedpreferences.SharedPreferencesHelper
import com.sensoft.aichawallet.helpers.sharedpreferences.SharedPreferencesHelperImpl
import com.sensoft.aichawallet.helpers.variant.VariantHelper
import com.sensoft.aichawallet.helpers.variant.VariantHelperImpl
import com.sensoft.aichawallet.models.activation.account.ActivationAccountRepository
import com.sensoft.aichawallet.models.activation.account.ActivationAccountRepositoryImpl
import com.sensoft.aichawallet.models.activation.request.ActivationRepository
import com.sensoft.aichawallet.models.activation.request.ActivationRepositoryImpl
import com.sensoft.aichawallet.token.TokenJWTService
import com.sensoft.aichawallet.token.TokenJWTServiceImpl
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import io.reactivex.rxkotlin.subscribeBy
import okhttp3.*
import okhttp3.logging.HttpLoggingInterceptor
import org.jetbrains.anko.runOnUiThread
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.io.IOException
import java.util.concurrent.TimeUnit


@Module
class GlobalInjectorModule(var activity: Activity) {

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideApplication(): AichaAppClass = activity.application as AichaAppClass

    @Provides
    fun provideVariantHelper(helper: VariantHelperImpl): VariantHelper = helper

    // Helper
    @Provides
    fun provideSharedPreferencesHelper(helper: SharedPreferencesHelperImpl): SharedPreferencesHelper = helper

    @Provides
    fun provideTokenJWTService(service: TokenJWTServiceImpl): TokenJWTService = service

    //Repos
    @Provides
    fun provideActivationRepository(repository: ActivationRepositoryImpl): ActivationRepository = repository

    @Provides
    fun provideActivationApiService(service: ActivationApiServiceImpl): ActivationApiService = service

    @Provides
    fun provideActivationAccountRepository(repository: ActivationAccountRepositoryImpl): ActivationAccountRepository = repository

    @Provides
    fun provideActivationAccountApiService(service: ActivationAccountApiServiceImpl): ActivationAccountApiService = service

    @Provides
    fun provideRetrofit(): Retrofit {
        val moshi = Moshi.Builder()
            .build()
        return Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
            .client(provideOkHttpClient())
            .baseUrl(provideVariantHelper(VariantHelperImpl()).getBackendEndPoint())
            .build()
    }

    @Provides
    fun provideOkHttpClient(): OkHttpClient {
        val okhttpClientBuilder = OkHttpClient.Builder()
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS)
        okhttpClientBuilder.followRedirects(false)

        okhttpClientBuilder.authenticator(TokenAuthenticator())

        //okhttpClientBuilder.addInterceptor(CustomInterceptor())
        okhttpClientBuilder.addInterceptor { chain ->

            var tokenValue = getToken(Constants.USER_TOKEN_KEY)

            Log.d(TAG, "provideOkHttpClient: token $tokenValue")

            if (tokenValue.isEmpty())
                tokenValue = "token"

            var builder = chain.request().newBuilder()
            var request = builder
                .addHeader("Authorization", "Bearer $tokenValue")
                .build()
            var response = chain.proceed(request)
            var tryCount = 0
            if (response.code == 401) {
                 provideContext().runOnUiThread {
                }
            } else if (response.code == 403) {
                provideContext().runOnUiThread {
                    provideApplication().onUnauthorized()
                }
                else -> {
                    while (response.code == 429 && tryCount < 3) {
                        Logger.d("429 : Request is not successful - $tryCount")
                        tryCount++
                        response = chain.proceed(request)
                    }
                }
            }

            response
        }

            okhttpClientBuilder.addInterceptor(object : NetworkConnectionInterceptor() {
                   override fun isInternetAvailable(): Boolean {
                       return AichaAppClass.isDeviceConnected
                   }

                   override fun onInternetUnavailable() {
                       provideContext().runOnUiThread {
                           provideApplication().onInternetUnavailable()
                       }
                   }
               })

               val interceptor = HttpLoggingInterceptor()
               interceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

               okhttpClientBuilder.addInterceptor(interceptor)

        return okhttpClientBuilder.build()

    }

    class TokenAuthenticator : Authenticator {
        @Throws(IOException::class)
        override fun authenticate(
            route: Route?,
            response: Response
        ): Request? {
            // Refresh your access_token using a synchronous api request
            var token : String = getTokenApi()

            if (token.isEmpty())
                token = "token"

            // Add new header to rejected request and retry it
            return response.request.newBuilder()
                .header("Authorization", "Bearer $token")
               // .header("Authorization", "Bearer " + getToken(Constants.USER_TOKEN_KEY))
                .build()
        }

        private fun getToken(key: String): String {
            var token: String? = null
            SharedPref.run {
                getString(AichaAppClass.getAppInstance(), key, "").subscribeBy(

                    onSuccess = {
                        token = it

                    },
                    onError = {

                    }
                )
            }

            return token.toString()
        }


        private fun getTokenApi() : String {

            val client = OkHttpClient.Builder()
                .build()


            val moshi = Moshi.Builder()
                .build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
                .client(client)
                .baseUrl(BuildConfig.TOKEN_URL)
                .build()
            var token = ""
            synchronized(this){
                val  response = retrofit.create(TokenApi::class.java).token(BuildConfig.GRANT_TYPE, BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, BuildConfig.PASSWORD, BuildConfig.USERNAME, "").execute()
                token = response.body()?.access_token.toString()
                SharedPref.putString(AichaAppClass.getAppInstance(), Constants.USER_TOKEN_KEY, token)
                Logger.e(TAG, "getTokenApi: ${response.body().toString()} -- $token")
                Log.e(TAG, "getTokenApi: ${response.body().toString()} -- $token")
              //  Thread{
              //  }.start()
            }
            return token
        }
    }

    private fun getToken(key: String): String {
        var token: String? = null
        SharedPref.run {
            getString(AichaAppClass.getAppInstance(), key, "").subscribeBy(

                onSuccess = {
                    token = it

                },
                onError = {

                }
            )
        }

        return token.toString()
    }
    class CustomInterceptor: Interceptor{
        override fun intercept(chain: Interceptor.Chain): Response {
            var tokenValue = getToken(Constants.USER_TOKEN_KEY)

            Log.d(TAG, "provideOkHttpClient: token $tokenValue" )
            if (tokenValue.isEmpty())
                tokenValue = "token"

            var builder = chain.request().newBuilder()
            var request = builder
                .addHeader("Authorization", "Bearer $tokenValue")
                .build()
            var response = chain.proceed(request)
            var tryCount = 0

            if (response.code == 401) {
                AichaAppClass.getAppInstance().runOnUiThread {
                    getTokenApi()
                    request = builder.addHeader("Authorization", "Bearer " + getToken(Constants.USER_TOKEN_KEY)).build()
                    response = chain.proceed(request)
                }
            }else if (response.code == 403) {
                AichaAppClass.getAppInstance().runOnUiThread {
                    AichaAppClass.getAppInstance().onUnauthorized()
                }
            } else {
                while (response.code == 429 && tryCount < 3) {
                    Logger.d("429 : Request is not successful - $tryCount")
                    tryCount++
                    response = chain.proceed(request)
                }
            }

            return response
        }

        private fun getTokenApi() : String {

            val client = OkHttpClient.Builder()
                .build()


            val moshi = Moshi.Builder()
                .build()

            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(MoshiConverterFactory.create(moshi).asLenient())
                .client(client)
                .baseUrl(BuildConfig.TOKEN_URL)
                .build()
            var token: String
            synchronized(this){
                val  response = retrofit.create(TokenApi::class.java).token(BuildConfig.GRANT_TYPE, BuildConfig.CLIENT_ID, BuildConfig.CLIENT_SECRET, BuildConfig.PASSWORD, BuildConfig.USERNAME, "").execute()
                token = response.body()?.access_token.toString()
                SharedPref.putString(AichaAppClass.getAppInstance(), Constants.USER_TOKEN_KEY, token)
                Logger.e(TAG, "getTokenApi: ${response.body().toString()} -- $token")

            }
            return token
        }


        private fun getToken(key: String): String {
            var token: String? = null
            SharedPref.run {
                getString(AichaAppClass.getAppInstance(), key, "").subscribeBy(

                    onSuccess = {
                        token = it

                    },
                    onError = {

                    }
                )
            }

            return token.toString()
        }
    }

    companion object {
        private const val TAG = "GlobalInjectorModule"
    }
}

