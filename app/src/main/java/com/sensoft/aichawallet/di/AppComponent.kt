package com.sensoft.aichawallet.di

import com.sensoft.aichawallet.AichaAppClass
import dagger.Component


@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(application: AichaAppClass)
}